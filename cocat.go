package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

func version() {
	os.Stderr.WriteString(`cocat: v0.1.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: cocat [OPTION] COMMAND {ARGS} {':' COMMAND {ARGS}}
Concatenate multi command output on row-by-row basis

Options:
	--help       show this help message and exit
	--version    print the version and exit

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

func splitStringList(src []string, sep string) [][]string {
	lensrc := len(src)
	if lensrc == 0 {
		return nil
	}

	dst := make([][]string, 0, 16)
	start := 0
	for i, s := range src {
		if s == sep {
			dst = append(dst, src[start:i])
			start = i + 1
		}
	}
	if start != lensrc {
		dst = append(dst, src[start:])
	}
	if sep == src[lensrc-1] {
		dst = append(dst, src[lensrc-1:lensrc-1])
	}
	return dst
}

func parseToCommandList(src [][]string) (dst []*exec.Cmd, err error) {
	dst = make([]*exec.Cmd, 0, 8)
	for _, c := range src {
		switch len(c) {
		case 0:
			continue
		case 1:
			_, err = exec.LookPath(c[0])
			if err != nil {
				return nil, err
			}
			newC := exec.Command(c[0])
			dst = append(dst, newC)
		default:
			_, err = exec.LookPath(c[0])
			if err != nil {
				return nil, err
			}
			newC := exec.Command(c[0], c[1:]...)
			dst = append(dst, newC)
		}
	}
	return dst, nil
}

func _main() (exitCode int, err error) {
	var isHelp, isVersion bool
	flag.BoolVar(&isHelp, "help", false, "show this help message and exit")
	flag.BoolVar(&isVersion, "version", false, "print the version and exit")
	flag.Parse()

	if isHelp {
		usage()
		return 0, nil
	}
	if isVersion {
		version()
		return 0, nil
	}

	args := splitStringList(flag.Args(), ":")
	commandList, err := parseToCommandList(args)
	if err != nil {
		return 1, err
	}

	if len(commandList) < 1 {
		return 1, fmt.Errorf("no input command")
	}

	stdinRead, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		return 1, err
	}

	outputList := make([]*bufio.Scanner, 0, 8)
	for _, c := range commandList {
		c.Stdin = bytes.NewReader(stdinRead)

		out, err := c.StdoutPipe()
		if err != nil {
			return 1, err
		}
		defer out.Close()

		outputList = append(outputList, bufio.NewScanner(out))
	}

	for _, c := range commandList {
		if err := c.Start(); err != nil {
			return 1, err
		}
	}

	linebuf := make([]string, len(commandList))
	for {
		for i, s := range outputList {
			if !s.Scan() {
				return 0, nil
			}
			linebuf[i] = s.Text()
		}
		fmt.Println(strings.Join(linebuf, ""))
	}
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "cocat:", err)
	}
	os.Exit(exitCode)
}

cocat
====================
Concatenate command output on a row-by-row basis

Usage
====================
	cocat [OPTION] COMMAND {ARGS} {':' COMMAND {ARGS}}

	Options:
    	--help     display this help and exit
    	--version  output version information and exit

notice
====================
* commands split by ":".

* It will work a until the end of any command.

* First, it read all standard input.
  "yes | cocat cat" doesn't work.

Example
====================
	$ cocat seq 1 10 : yes " " : ls
	1 foo
	2 bar
	3 baz
	4 hoge
	5 piyo
	6 fuga

	$ ls | cocat seq 1 10 : yes " " : grep b
	1 bar
	2 baz

	$ ls | cat : cat : cat
	foofoofoo
	barbarbar
	bazbazbaz
	hogehogehoge
	piyopiyopiyo
	fugafugafuga

License
====================
MIT License

author
====================
Kusabashira <kusabashira227@gmail.com>
